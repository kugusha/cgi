#include <sys/socket.h>

#ifndef STATIC_H
#define STATIC_H

void catch(int client_socket, struct sockaddr_in * client_address);
void server(char* host, int port) ;

#endif /* STATIC_H */
