#include "protocol.h"
// \_HEADER
// |_getUrl
#include "static.h"
// \_checkFile

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdbool.h>

void catch(int client_socket, struct sockaddr_in * client_address) {
  // ===========================
  // ==== GET REQUEST INFO =====
  // ===========================
  char request_CONTENT[4096];
  char url[4096];

  char *client_ip = inet_ntoa(client_address->sin_addr);
  int client_port = ntohs(client_address->sin_port);
  read(client_socket, request_CONTENT, sizeof(request_CONTENT));
  get_URL(request_CONTENT, url);

  printf("|i| Request for %s \t%s:%d\n", url, client_ip, client_port);

  // '/' is equal to '/index.html'
  if (strcmp("/", url) == 0) {
    memset(url, 0, sizeof(url));
    strcpy(url, "/index.html");
  }

  // ===========================
  // CHECK FILE AND WRITE HEADER
  // ===========================
  char response_HEADERS[8192]; // 8 kB should be enough (c) Bill Gates
  int fileSize = checkFile(url, PATH_TO_STATIC);

  // Explanation : negative value means that file doesn't exists
  // abs(value) is the size of '404.html'
  // Otherwise size of the file will be returned
  if (fileSize < 0) { // Not found
    printf("|-| File '%s' missed \tContent-length : %d\n", url, -fileSize);
    sprintf(response_HEADERS, HEADER, 404, "Not found", -fileSize);

    memset(url, 0, sizeof(url));
    strcpy(url, "/404.html");
  } else { // Exists
    printf("|+| File '%s' exists \tContent-length : %d\n", url, fileSize);
    sprintf(response_HEADERS, HEADER, 200, "OK", fileSize);
  }

  // ===========================
  //   WRITE RESPONSE TO SOCKET
  // ===========================
  write(client_socket, response_HEADERS, strlen(response_HEADERS)); // Headers
  sendFileToSocket(url, PATH_TO_STATIC, client_socket);
}

void server(char* host, int port) {
  int server_socket;
  int client_socket;
  struct sockaddr_in server_address;
  struct sockaddr_in client_address;

  // ===========================
  // ====== CONFIG SERVER ======
  // ===========================
  server_socket = socket(AF_INET, SOCK_STREAM, 0);
  server_address.sin_addr.s_addr = inet_addr(host);
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(port);

  // ===========================
  // ======= RUN SERVER ========
  // ===========================
  socklen_t client_address_size = sizeof(client_address);
  bind(server_socket, (struct sockaddr*) &server_address, sizeof(server_address));
  listen(server_socket, 10);
  while (client_socket =  accept(server_socket,
                          (struct sockaddr*) &client_address,
                          &client_address_size)) {
    // ===========================
    // ========= FORKING =========
    // ===========================
    pid_t pid = fork();
    if (pid == 0) { catch(client_socket, &client_address); exit(0); }
    else { close(client_socket); }
  }
}
