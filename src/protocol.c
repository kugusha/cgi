#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

char* HEADER =
  "HTTP/1.1 %d %s\r\n" // 'HTTP/1.0 200 OK' for example
  "Server : customCGI\r\n"
  "Content-type : text/html\r\n"
  "Content-length : %d\r\n" // 'Content-length : 1500'
  "\r\n";

char* PATH_TO_STATIC = "/var/www/CGI_static";

void get_URL(char *buffer, char * url) {
  memset(url, 0, sizeof(url)); // clear 'url' variable
  char *to = strstr(buffer, "HTTP/") - 1;
  strncpy(url, buffer + 4, to - (buffer + 4));
}
