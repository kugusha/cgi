#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/sendfile.h>

int fileSize(int file) { // Get file size (works if file exists)
  struct stat st;
  fstat(file, &st);
  return st.st_size;
}

int checkFile(char* name, char* staticPath) {
  // ===========================
  // ====== GET FULL PATH ======
  // ===========================
  char path[4096];
  strcpy(path, staticPath);
  strcat(path, name);

  // ===========================
  // ======= OPEN FILE =========
  // ===========================
  int file;
  file = open(path, O_RDONLY);
  if (file < 0) { // File doesn't exist
    close(file);

    strcpy(path, staticPath);
    strcat(path, "/404.html");
    file = open(path, O_RDONLY);

    int size = -1 * fileSize(file);
    close(file);
    return size;
  } else { // File exists
    int size = fileSize(file);
    close(file);
    return size;
  }
}

void sendFileToSocket(char* name, char* staticPath, int clientSocket) {
  char path[4096];
  strcpy(path, staticPath);
  strcat(path, name);

  // printf("=== Full path to file: %s\n", path);

  int file = open(path, O_RDONLY);
  off_t offset = 0;
  size_t size = (size_t)fileSize(file);

  // printf("=== Size of the file: %zu\n", size);

  sendfile(clientSocket, file, &offset, size);

  close(file);
  close(clientSocket);
}
