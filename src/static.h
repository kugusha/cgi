#ifndef STATIC_H
#define STATIC_H

int checkFile(char* name, char* staticPath);
int fileSize(int file);
void sendFileToSocket(char* name, char* staticPath, int clientSocket);

#endif /* STATIC_H */
