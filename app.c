#include "src/start.h"
// \_server

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>

int isValidIpAddress(char *ipAddress) {
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
    return result;
}

int main(int argc, char *argv[]) {
  puts("[i] CGI server by Kugusheva Alexandra");
  puts("[i] 2017 - present, licensed by WTFPL");

  // ===========================
  // ===== CHECK ARGUMENTS =====
  // ===========================
  if (argc != 3) { // Check number of arguments
    puts("|-| Your usage format is wrong!");
    puts("|-| Please use : ./run ip port");
    puts("|-| For example : ./run 127.0.0.1 5000");
    return 127;
  } else if (isValidIpAddress(argv[1]) == 0) { // Check IP
    puts("[-] Specified IP address is invalid!");
    return 127;
  } else if (atoi(argv[2]) >= 65535 && 1 >= atoi(argv[2])) { // Check port
    puts("[-] Specified port is invalid");
    return 127;
  }

  // ===========================
  // ========== RUN ============
  // ===========================
  server(argv[1], atoi(argv[2]));

  return 0;
}
