*Кугушева Александра*

*Группа 156*



# **Отчет по проектной работе «Веб-сервер с функциональностью CGI»**



#### Структура:

1. Введение в предметную область, актуальность работы, обзор существующих решений. -✔️

2. Описание проделанной работы: - ✔️

     2.1. Описание архитектуры программного продукта

     2.2. Инструкция по компиляции, установке, настройке и запуску

     2.3. Готовая документация - руководство пользователя; целевая аудитория этой документации - это человек, не имеющий никакого отношения к проекту

3. Результат - что сделано, сравнение с конкурентами. - ✖️ (должно быть сделано к 3 КТ)



##### *Что такое веб-сервер?*

[**Веб-сервер**](https://ru.wikipedia.org/wiki/Веб-сервер) — [сервер](https://ru.wikipedia.org/wiki/%D0%A1%D0%B5%D1%80%D0%B2%D0%B5%D1%80_(%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5)), принимающий HTTP-запросы от клиентов, обычно [веб-браузеров](https://ru.wikipedia.org/wiki/%D0%91%D1%80%D0%B0%D1%83%D0%B7%D0%B5%D1%80), и выдающий им HTTP-ответы, как правило, вместе с HTML-страницей, изображением, файлом, медиа-потоком или другими данными. Подробнее можно прочитать [здесь](https://en.wikipedia.org/wiki/Web_server). 

![Simple diagram of CGI](http://www.oreilly.com/openbook/cgi/figs/cgi0101.gif)

##### *Что такое CGI?*

[Common Gateway Interface (CGI)](https://ru.wikipedia.org/wiki/CGI) - стандарт простого интерфейса, используемого для взаимодействия между веб-сервером и сторонними программами, генерирующий динамический контент. 

Интерфейс разработан таким образом, чтобы можно было использовать любой язык программирования, который может работать со стандартными устройствами ввода-вывода. В нашем случае используется Си. 



CGI-сервер широко используется для решания задач, не требующих никакой сложной функциональности. Например, формы регистрации, обратной связи или форма отправки комментария. Еще один вариант использования - это работа с [cookie](https://ru.wikipedia.org/wiki/Cookie): сбор данных о пользовательском браузере, подсчет количества посещений веб-страницы.


Простейший пример CGI скрипта на C:

```c++
include <stdio.h>

int main(void) {
  printf("Content-Type: text/plain\r\n"); // http-заголовок, задающий тип содержимого (mime-type). \r\n — обязательный символ, отделяющий заголовки от тела сообщения.
  printf("Hello, world!\r\n");
  
  return 0;
}
```

##### Обзор некоторых существующих решений:

- [Python CGI](https://docs.python.org/2/library/cgi.html) - встроенный CGI сервер для Python 3.
- [Perl CGI](http://perldoc.perl.org/CGI.html)



### Описание проделанной работы

##### *Описание архитектуры программного продукта*

Структура проекта:
```
.
├── app.c // Точка входа
├── build // Bash скрипт для сборки и установки
├── Jmeter_load_testing.jmx // Конфигурация для тестирования в Apache Jmeter
├── jmeter_results.csv // Результаты тестирования
├── README.md
├── REPORT.md
├── src
│   ├── protocol.c // Утилиты для работы с протоколом HTTP
│   ├── protocol.h
│   ├── start.c // Утилиты для создания и запуска сервера
│   ├── start.h
│   ├── static.c // Утилиты для работы со статическими страницами
│   └── static.h
└── tmp
    └── static // Предустановленный набор файлов
        ├── 404.html
        └── index.html
```

- Сервер обслуживает одновременно несколько подключений, за счет чего достигается большая производительность. Форкинг происходит в `srr/start.c/server`.

- Статические страницы расположены в `tmp/static`, при установке копируются в `/var/www/CGI_server`, поэтому возможно использование на различных Linux дистрибутивах. Путь к папке со статикой можно указать в `src/protocol.c/PATH_TO_STATIC`.

- По дефолту, запрос на `server:port/` воспринимается сервером как запрос к главной странице (`index.html`). Во всех остальных случаях требуется в качестве URL указывать путь к файлу из папки `/var/www/CGI_server/static`. Если запрашиваемый файл не существует или возникли проблемы с открытием/чтением файла, то вернется страница `404.html`.

- На данный момент реализовано два вида заголовков (шаблон заголовка находится в `src/protocol.c`). Пример:

```
  HTTP/1.0 200 OK
  Server : customCGI
  Content-type : text/html
  Content-length : 1500
```

- В зависимости от результата запроса, будет возвращен заголовок `HTTP/1.0 200 OK` или `HTTP/1.0 404 Not found`.

- В проекте используется самописный логгер с синтаксисом:

	- `[i]` - общая информация
	- `[+]` - уведомление об успехе (например файл найден)
	- `[-]` - уведомление об ошибке (например неправильный формат порта)

- Для записи файла в сокет был использован POSIX вызов [sendfile()](http://man7.org/linux/man-pages/man2/sendfile.2.html), вместо привычного подхода `чтение -> буферизация -> запись`. За счет этого также была увеличена производительность.

##### Инструкция по компиляции, устновке

1. В первую очередь необходимо скачать репозиторий с исходниками. Для этого предпочтительно использовать `git`. Для Debian-based дистрибутивов устанавливается как:

    ```
    $ sudo apt-get update
    $ sudo apt-get install git
    ```

    Далее скачиваем репозиторий и переходим в него:

    ```
    $ git clone https://bitbucket.org/kugusha/cgi
    $ cd cgi/
    ```

2. Следующим этапом надо запустить установочный скрипт `build`. Так как по дефолту папка со статикой копируется в /var/www, то необходимо запустить build с root правами.

    ```
    $ sudo ./build
    [i] Checking for gcc package installed: install ok installed
    [+] Build successfully
    [i] Can't find static folder ('/var/www/CGI_static')
    [i] Copying preinstalled static folder from the 'tmp/'
    [i] Also you can specify static folder in the 'src/protocol.c'
    ```

3. Если установка прошла без ошибок, то был создан исполняемый файл `run`; статическое содержимое теперь находится в `/var/www/CGI_server`. Для запуска сервера нужно указать в аргументах IP адрес и порт:

    ```
    $ ./run 127.0.0.1 8000
    [i] CGI server by Kugusheva Alexandra
    [i] 2017 - present, licensed by WTFPL
    ```

	Теперь сервер работает и принимает запросы к `127.0.0.1:8000`. На каждый запрос в консоль будет выводится лог, с результатом обработки:

	```
    |i| Request for / 	127.0.0.1:33608
	|+| File '/index.html' exists 	Content-length : 1911
	```

	Вот так лог выглядит, если файл был не найден:

    ```
    |i| Request for /favicon.ico 	127.0.0.1:33616
    |-| File '/favicon.ico' missed 	Content-length : 4200
	```

### Документация

##### Описание исходников

Ниже кратко описан исходный код проекта. Заголовочные файлы не представляют интереса, поэтому они пропущены.

```
.
├── app.c
├── src
│   ├── protocol.c
│   ├── start.c
│   ├── static.c
```

- `app.c`
	Точка входа находится здесь. В первую очередь проверяет полученные аргументы на валидность (`int isValidIpAddress(char *ipAddress)`) и, если все корректно, - запускает сервер (`src/start.c/void server(char* host, int port)`).

- `src/start.c`
	- `void server(char* host, int port)`
		Пытается запустить сервер на `host:port`. Если удалось - запускает бесконечный цикл, в процессе которго слушает server_socket на предмет входящих запросов.
    - `void catch(int client_socket, struct sockaddr_in * client_address)`
    	Обрабатывает полученный запрос: получает `ip:port` клиента, вытаскивает URL из заголовка запроса (`get_URL(request_CONTENT, url)`), проверяет наличие запрашиваемого файла (`int fileSize = checkFile(url, PATH_TO_STATIC)`), в зависимости от результата пишет заголовок ответа и записывает файл в сокет (`sendFileToSocket(url, PATH_TO_STATIC, client_socket)`)

- `src/prtocol.c`
	- `char* HEADER`
		Щаблон HTTP заголовка, используемый для ответа
	- `char* PATH_TO_STATIC`
		Путь к папке со статикой
	- `void get_URL(char *buffer, char * url)`
		Вспомогательная функция, парсит заголовки запроса на предмет URL и записывает его в char* url 

- `src/static.c`
	- `int fileSize(int file)`
		Возвращет размер файла
	- `int checkFile(char* name, char* staticPath)`
		Проверяет наличие файла. Если существует - возвращает его размер, если нет - размер файла `404.html` со знаком минус.
	- `void sendFileToSocket(char* name, char* staticPath, int clientSocket)`
		Пишет файл напрямую в сокет клиента с помощью `sendfile()`

### Фоновый режим

- Для запуска в фоновом режиме рекомендуется использовать утилиту screen

```
$ sudo apt-get install screen
```

### Нагрузочное тестирование

Сервер был протестирован с помощью Apache Jmeter. Конфиг файл находится в корне `/Jmeter_load_testing.jmx`. Тестирование проводилось с параметрами:
- `Number of threads` : 10000
- `Ramp-Up peroid` : 1 second

В качестве endpoint использовался корень каталога, то есть `index.html`. Сервер показал отличные результаты: все 10000 ответов были корректны, а среднее время ответа равняется 81 ms. Подробный отчет в формате csv находится в файле `jmeter_results.csv`